package com.example.practicaa42;

import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdaptadorEmpleadoHolder extends RecyclerView.ViewHolder {

    ImageView imageView;
    TextView nombre, cargo;

    public AdaptadorEmpleadoHolder(@NonNull View itemView) {
        super(itemView);
        imageView = itemView.findViewById(R.id.imageView);
        nombre = itemView.findViewById(R.id.tvNombre);
        cargo = itemView.findViewById(R.id.tvCargo);
    }


    public void imprimir(int position) {
        imageView.setImageResource(tasca2.fotos[position]);
        nombre.setText(tasca2.noms[position]);
        cargo.setText(tasca2.carrecs[position]);
    }
}
