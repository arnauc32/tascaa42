package com.example.practicaa42;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class tasca2 extends AppCompatActivity {
    RecyclerView recyclerView;
    static String[] noms = {"Arnau Carbonell","Marc Carbonell","Xavi Mira","Fernando Alonso"};
    static String[] carrecs = {"Programador", "Analista","Recursos Humans","CEO"};
    public static int[] fotos = {R.drawable.mujer,R.drawable.mujer1,R.drawable.mujer2,R.drawable.hombre};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tasca2);

        recyclerView = findViewById(R.id.recycler);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        recyclerView.setAdapter(new AdaptadorEmpleado());
    }

}
