package com.example.practicaa42;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class AdaptadorEmpleado extends RecyclerView.Adapter<AdaptadorEmpleadoHolder> {
    @NonNull
    @Override
    public AdaptadorEmpleadoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new AdaptadorEmpleadoHolder((getLayoutInflater().inflate(R.layout.card_view,parent,false)));
    }

    @Override
    public void onBindViewHolder(@NonNull AdaptadorEmpleadoHolder holder, int position) {
        holder.imprimir(position);
    }

    @Override
    public int getItemCount() {
        return tasca2.noms.length;
    }
}
