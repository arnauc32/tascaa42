package com.example.practicaa42;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.TableLayout;
import android.widget.TextView;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TabLayout tabs=findViewById(R.id.tabLayout);
        tabs.addTab(tabs.newTab().setText("Roig"));
        tabs.addTab(tabs.newTab().setText("Verd"));
        tabs.addTab(tabs.newTab().setText("Blau"));

        TextView text = findViewById(R.id.textView);
        text.setText("Roig");
        text.setTextColor(Color.RED);

        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()){
                    case 0:
                        text.setText("Roig");
                        text.setTextColor(Color.RED);
                        text.setBackgroundColor(Color.RED);
                        break;
                    case 1:
                        text.setText("Verd");
                        text.setTextColor(Color.GREEN);
                        text.setBackgroundColor(Color.GREEN);
                        break;
                    case 2:
                        text.setText("Blau");
                        text.setTextColor(Color.BLUE);
                        text.setBackgroundColor(Color.BLUE);
                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}